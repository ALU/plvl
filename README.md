Ansible playbook for kvm VM attachment to Nuage SDN when there are no CMS in your environment.


Requirements
------------

No requirements

Role Variables
--------------

`src_qcow2_path` - path to source image file for VM.

`install_packages` - when yes libvirt and other packages will be installed.

`vm_name` - VM name for libvirt (will not be propagated inside VM).

`cpu_number` - number of vCPU to be allocated for VM.

`memory_size` - amount of memory to be allocated for VM.

`nuage_*` - Nuage metadata required for VM identity at VSD. VRS recieves this metadata from libvirt callback and send to VSC.

Dependencies
------------

No dependencies.

Example Playbook
----------------

```yaml

---
- name: deploy virtual machine 
  gather_facts: no
  hosts: localhost
  vars:
    # Set yes if want to install libvirt tot localhost
    install_packages: no
    # path to source image file for VM
    src_qcow2_path: /root/centos7-template-dhcp.qcow2
    # VM hostname. Is going to be used for virsh vm definition
    vm_name: centos-2
    # Number of vCPU to allacote for VM
    cpu_number: 2
    # Amount of memory in gb to allacote for VM
    memory_size: 2
    # Nuage parameters
    nuage_enterprise: ag
    nuage_user:       admin
    nuage_domain:     dom1
    nuage_zone:       zone1
    nuage_subnet:     sub1
  tasks:

    - name: List the Virtual Machine
      virt: command=list_vms
      register: virt_vms

    - name: Check if VM already exists
      fail: msg="The VM {{ vm_name }} is already defined on this hypervisor."
      when: vm_name in virt_vms.list_vms

    - include: install-packages.yml
      when: install_packages

    - include: prepare-image.yml
    - include: prepare-xml.yml
    - include: start-vm.yml
```

Author Information
------------------

[Aleksey Gorbachev](https://gitlab.com/u/agorbachev)



